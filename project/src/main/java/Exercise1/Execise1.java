package Exercise1;

public class Execise1 {

	public static int exerciseOne(int i, int e) {
		int sumOfInt = i+e;
		int thisIsSimple = sumOfInt * 10;
		return thisIsSimple;
	}
	
	public static void main(String[] args) {
		System.out.println(exerciseOne(10,20));
		System.out.println(exerciseOne(15,-30));
		System.out.println(exerciseOne(-5,-7));
	}
}
